<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculator</title>
    <!-- bootstrap css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>


<body>
    
   <section class="container">
   <div class="row d-flex justify-content-left">
    
   <form action="">
        <input type="text" name="num1" placeholder="give a number">
        <input type="text" name="num2" placeholder="give another number">
        <select name="operator">
            <option>NONE</option>
            <option>ADD</option>
            <option>SUBSTRACT</option>
            <option>MULTIPLY</option>
            <option>DIVIDE</option>
      </select>
      <br>
      <button class="btn btn-success mt-3 mb-3" type="submit" name="submit" value="submit" >CALCULATE</button>
    </form>

    <p>The answer is: </p>
    <?php
   
    function add($num1,$num2){
        echo $num1+$num2;
    }
    function substract($num1,$num2){
        echo $num1-$num2;
    }
    function multiply($num1,$num2){
        echo $num1*$num2;
    }
    function division($num1,$num2){
        echo $num1/$num2;
    }
    if(isset($_GET['submit'])){
        $result1=$_GET['num1'];
        $result2=$_GET['num2'];
        $operator=$_GET['operator'];
        switch($operator){
            case "NONE":
                echo "You need to select a operator!";
                break;
            case "ADD":
                add($result1,$result2);
                break;
            case "SUBSTRACT":
                substract($result1,$result2);
                break;
            case "MULTIPLY":
                multiply($result1,$result2);
                break;
            case "DIVIDE":
                division($result1,$result2);
                break;

        }
    }
    ?>

   </div>
   </section>

<!-- bootstrap bundle -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>